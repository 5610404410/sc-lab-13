package Main;

import Model.Queue;

public class ProducerConsumerMain {

	public static void main(String[] args) {
				
		Queue q = new Queue();

		Producer producer = new Producer(q);
		Consumer consumer = new Consumer(q);
		
		Thread r1 = new Thread(producer);

		Thread r2 = new Thread(consumer);
		
		r1.start();
		r2.start();
	}

}
