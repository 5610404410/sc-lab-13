package Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Queue {
	private Lock lock;
	public Condition sufficient;
	
	
	private ArrayList<String> list;
	
	public Queue(){
		lock = new ReentrantLock();
		sufficient = lock.newCondition();
		list = new ArrayList<String>();
	}
	
	public void enqeue(String str){
		lock.lock();

		//System.out.println(new Date().toString());
		while(getList().size() == 10){
			
			try {
				sufficient.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println(new Date().toString());
		
		list.add(str);
		
		sufficient.signal();
		lock.unlock();
		}
	
	public String deqeue(){
		lock.lock();
		while(getList().size() == 0){
			try {
				sufficient.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
			
		
		String i = list.remove(0);
		System.out.println(i);
		sufficient.signal();
		lock.unlock();

		
		return i;
	}
	
	public ArrayList getList(){
		return list;
	}

}
